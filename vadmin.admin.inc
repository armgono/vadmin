<?php

function vadmin_main_settings_form() {
  return array();
}

/**
 * Menu callback; Provides the administration overview page.
 */
function vadmin_config_page() {
  $blocks = array();
  $links = array(
    'vadmin_content' => array(
      'link' => 'content',
      'title' => t('Administration content settings'),
      'description' => t('Administration pages of site content block and content view settings')
    ),
    'vadmin_blocks' => array(
      'link' => 'blocks',
      'title' => t('Administration blocks\' settings'),
      'description' => t('Administration pages sidebar blocks\' generation')
    ),
    'vadmin_code' => array(
      'link' => 'code',
      'title' => t('External code'),
      'description' => t('Add external code to site')
    ),
    'vadmin_pages' => array(
      'link' => 'pages',
      'title' => t('Administration pages settings'),
      'description' => t('Administration pages\' generation settings')
    ),
    'vadmin_404' => array(
      'link' => '404',
      'title' => t('404 page settings'),
      'description' => t('Set logo for 404 page')
    ),
    'vadmin_defaults' => array(
      'link' => 'defaults',
      'title' => t('Default settings'),
      'description' => t('Set defaults\' settings for project')
    )
  );
  foreach ($links as $module_name => $block) {
    if (module_exists($module_name)) {
      $block_content['content'][0] = array(
        'title' => $block['title'],
        'description' => $block['description'],
        'href' => 'admin/vadmin/' . $block['link'],
        'localized_options' => array()
      );
      $blocks[] = array(
        'title' => $block['title'],
        'description' => $block['description'],
        'content' => theme('admin_block_content', $block_content),
        'show' => TRUE
      );
    }
  }
  if ($blocks) {
    return theme('admin_page', array('blocks' => $blocks));
  }
  else {
    return t('You do not have any administrative item.');
  }
}
