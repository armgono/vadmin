<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Страница не найдена - ошибка 404 | Page not found - error 404</title>
    <meta http-equiv="Content-Language" content="ru" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="author" content="Веб-cтудия Voodoo.ru" />
    <style>
      *{margin:0;padding:0; font-family:Arial, Verdana;font-size:12px}
      html{height:100%}
      body{height:100%}
      a img{border:none}
      p{margin-bottom:10px}
      #wrap{min-height:100%;overflow:hidden;position:relative;_height:100%;_overflow:visible}
      #info{position:absolute;width:900px;height:240px;top:50%;margin-top:-120px;left:50%;margin-left:-300px}
      #info ul {list-style-type:none}
      #info ul li{float:left;margin:0 15px 20px 0;width:40%}
      #info ul li:first-child{text-align:center}
      #footer{position:absolute;bottom:-0;padding:20px 0;border-top:2px solid #ccc;width:900px;left:50%;margin-left:-450px}
    </style>
  </head>
  <body>
    <div id="wrap">
      <div id="info">
        <ul>
          <li><a href="/" title="На главную"><img src="<?php print $logo404; ?>" alt="Page not found" /></a></li>
          <li>
            <p><img src="/<?php print $img404 ?>" /></p>
            <p>Страница не найдена или уже не существует.</p>
            <p><a href="/">На главную</a>&nbsp;&nbsp;&nbsp;<span style="color:#ccc">|</span>&nbsp;&nbsp;&nbsp;<a href="/sitemap">Карта сайта</a>
          </li>
        </ul>
      </div>
      <div id="footer"></div>
    </div>
  </body>
</html>