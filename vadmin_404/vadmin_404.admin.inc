<?php

function vadmin_404_form($form, $form_state) {
  $form = array();
  $form['vadmin_404_logo'] = array(
    '#type' => 'managed_file',
    '#title' => t('Logo for 404'),
    '#desription' => t('Upload logo for 404 error page'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('png gif jpg')
    ),
    '#upload_location' => 'public://vadmin/404/',
    '#default_value' => vadmin_get_data('vadmin_404_logo', ''),
  );
  $form['#submit'][] = 'vadmin_404_form_submit_callback';
  return vadmin_settings_form($form);
}

function vadmin_404_form_submit_callback(&$form, &$form_state) {
  $file = file_load($form_state['values']['vadmin_404_logo']);
  if ($file) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'vadmin_404', 'vadmin_404_logo', 1);
  }
}
