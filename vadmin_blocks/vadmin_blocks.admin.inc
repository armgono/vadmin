<?php

function vadmin_blocks_settings_form($form, $form_state) {
  $form = array();
  $groups = vadmin_blocks_get_groups($reset = TRUE);
  if (empty($groups)) {
    /* $form['info'] = array(
      '#type' => 'markup',
      '#markup' => t('Please !create blocks first', array('!create' => l(t('Create block'), 'admin/vadmin/blocks/add')))
      ); */
  }

  $form['groups'] = array('#tree' => TRUE);
  $form['links'] = array('#tree' => TRUE);
  foreach ($groups as $group) {
    $gid = $group->id;
    $form['groups'][$gid]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#delta' => 25,
      '#title_display' => 'invisible',
      '#default_value' => $group->weight,
    );
    $form['groups'][$gid]['title'] = array(
      'title' => array('#markup' => $group->subject)
    );
    $form['groups'][$gid]['id'] = array(
      '#type' => 'value',
      '#value' => $gid
    );
    $action_links = array(
      l(t('Add link'), 'admin/vadmin/blocks/link/add/' . $gid),
      l(t('Delete group'), 'admin/vadmin/blocks/block/' . $gid . '/delete')
    );
    $form['groups'][$gid]['actions'] = array(
      '#theme' => 'item_list',
      '#items' => $action_links,
      '#type' => 'ul'
    );
    $form['groups'][$gid]['parent'] = array(
      '#type' => 'textfield',
      '#default_value' => 0,
      '#size' => 3,
    );
    $form['groups'][$gid]['current_id'] = array(
      '#type' => 'textfield',
      '#default_value' => $gid,
      '#disabled' => TRUE,
      '#size' => 3,
    );
    $links = vadmin_blocks_get_links($gid);

    foreach ($links as $link) {
      $action_links = array(
        l(t('Edit'), 'admin/vadmin/blocks/link/edit/' . $link->id),
        l(t('Delete'), 'admin/vadmin/blocks/link/delete/' . $link->id)
      );
      $form['links'][$gid][$link->id]['weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#delta' => 25,
        '#title_display' => 'invisible',
        '#default_value' => $link->weight,
      );
      $form['links'][$gid][$link->id]['title'] = array(
        'title' => array('#markup' => l($link->title, $link->href))
      );
      $form['links'][$gid][$link->id]['actions'] = array(
        '#theme' => 'item_list',
        '#items' => $action_links,
        '#type' => 'ul'
      );
      $form['links'][$gid][$link->id]['parent'] = array(
        '#type' => 'textfield',
        '#default_value' => $gid,
        '#size' => 3,
      );
      $form['links'][$gid][$link->id]['current_id'] = array(
        '#type' => 'textfield',
        '#default_value' => $link->id,
        '#disabled' => TRUE,
        '#size' => 3,
      );
    }
  }
  $form['new'] = vadmin_blocks_block_edit_form();
  $form['new']['#tree'] = TRUE;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['#theme'] = 'vadmin_blocks_list';
  return $form;
}

function vadmin_blocks_settings_form_submit($form, $form_state) {
  form_state_values_clean($form_state);
  $groups = $form_state['values']['groups'];
  if (isset($form_state['values']['links'])) {
    $links = $form_state['values']['links'];
    foreach ($links as $gid => $link) {
      vadmin_blocks_links_save($link);
    }
    $message = t('Links updated');
    drupal_set_message($message);
    watchdog('vadmin', $message);
  }
  foreach ($groups as $id => $group) {
    drupal_write_record('vadmin_blocks_group', $group, 'id');
  }
  block_flush_caches();
}

function vadmin_blocks_block_edit_form() {
  $form = array();
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Blocks group subject'),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 25,
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('vadmin_blocks_block_edit_form_submit'),
    '#validate' => array('vadmin_blocks_block_edit_form_validate')
  );
  return $form;
}

function vadmin_blocks_block_edit_form_validate(&$form, &$form_state) {
  $subject = trim($form_state['values']['new']['subject']);
  if (empty($subject)) {
    form_set_error('new][subject', t('For adding group, complate the set group subject value'));
  }
}

function vadmin_blocks_block_edit_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $group = (object) $form_state['values']['new'];
  vadmin_blocks_group_save($group);
}

function vadmin_blocks_block_delete_form($form, $form_state, $gid) {
  $form = array();
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $gid,
  );
  $group = vadmin_blocks_get_group($gid);
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $group->subject,
  );
  $question = t('Are you sure you want to delete group %group?', array('%group' => $group->subject));
  $path = 'admin/vadmin/blocks';
  $description = t('All links contains in this group will be deleted. This action cannot be undone.');
  $yes = t('Delete');
  $no = t('Cancel');
  return confirm_form($form, $question, $path, $description, $yes, $no);
}

function vadmin_blocks_block_delete_form_submit($form, &$form_state) {
  vadmin_blocks_block_delete($form_state['values']['id']);
  $message = t('Group %group_name deleted', array('%group_name' => $form_state['values']['name']));
  $message .= "\n" . t('All links in group %group_name deleted', array('%group_name' => $form_state['values']['name']));

  drupal_set_message(nl2br($message));
  watchdog('vadmin', $message);
  $form_state['redirect'] = 'admin/vadmin/blocks';
}

function vadmin_blocks_link_edit_form($form, $form_state, $action, $object) {
  $form = array();
  $form['action'] = array('#type' => 'value', '#value' => $action);
  if ($action == 'delete') {
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $object->id,
    );
    $form['name'] = array(
      '#type' => 'value',
      '#value' => $object->title,
    );
    $question = t('Are you sure you want to delete link %link?', array('%link' => $object->title));
    $path = 'admin/vadmin/blocks';
    $description = t('This action cannot be undone.');
    $yes = t('Delete');
    $no = t('Cancel');

    return confirm_form($form, $question, $path, $description, $yes, $no);
  }
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Link title'),
    '#required' => TRUE
  );
  $form['href'] = array(
    '#type' => 'textfield',
    '#title' => t('Link href'),
    '#required' => TRUE
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 25,
  );
  $form['actions'] = array('#type' => 'actions');

  if ($action == 'add') {
    $form['bid'] = array(
      '#type' => 'value',
      '#value' => $object->id
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add')
    );
  }
  elseif ($action == 'edit') {
    if ($object) {
      foreach ($form as $item_name => $form_item) {
        if (!empty($object->{$item_name})) {
          $form[$item_name]['#default_value'] = $object->{$item_name};
        }
      }
      $form['id'] = array(
        '#type' => 'value',
        '#value' => $object->id
      );
      $form['bid'] = array(
        '#type' => 'value',
        '#value' => $object->bid
      );
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Edit')
      );
    }
    else {
      drupal_set_message(t('Form is incorrect, contact to admin', 'error'));
    }
  }


  return $form;
}

function vadmin_blocks_link_edit_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);

  if ($form_state['values']['action'] == 'delete') {
    vadmin_blocks_delete_link($form_state['values']['id']);
    drupal_set_message(t('Link %link_title deleted', array('%link_title' => $form_state['values']['name'])));
  }
  else {
    unset($form_state['values']['action']);
    $link = (object) $form_state['values'];
    vadmin_blocks_link_save($link);
  }
  $form_state['redirect'] = 'admin/vadmin/blocks';
}
