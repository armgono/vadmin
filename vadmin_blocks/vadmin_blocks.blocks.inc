<?php

function vadmin_blocks_group_view($group) {
  if (empty($group->links)) {
    return array();
  }
  $links = array();
  foreach ($group->links as $link) {
    if (vadmin_blocks_is_access($link)) {
      $links[] = $link;
    }
  }
  if ($links) {
    return array(
      '#theme' => 'vadmin_blocks_group',
      '#links' => $links
    );
  }
  else {
    return array();
  }
}
