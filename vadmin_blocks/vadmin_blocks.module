<?php

/**
 * implements hook_menu()
 */
function vadmin_blocks_menu() {
  $items = array();

  $items['admin/vadmin/blocks'] = array(
    'title' => 'Voodoo admin blocks settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vadmin_blocks_settings_form'),
    'access arguments' => array('vadmin root access'),
    'file' => 'vadmin_blocks.admin.inc',
    'type' => MENU_NORMAL_ITEM
  );
  $items['admin/vadmin/blocks/link/add/%vadmin_blocks_group'] = array(
    'title' => 'Add link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vadmin_blocks_link_edit_form', 'add', 5),
    'access arguments' => array('vadmin root access'),
    'file' => 'vadmin_blocks.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/vadmin/blocks/link/%/%vadmin_blocks_link'] = array(
    'title' => 'Add link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vadmin_blocks_link_edit_form', 4, 5),
    'access arguments' => array('vadmin root access'),
    'file' => 'vadmin_blocks.admin.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/vadmin/blocks/block/%/delete'] = array(
    'title' => 'Add block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vadmin_blocks_block_delete_form', 4),
    'access arguments' => array('vadmin root access'),
    'file' => 'vadmin_blocks.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 *
 * @return array of blocks group
 */
function vadmin_blocks_get_groups($reset = FALSE) {
  $groups = &drupal_static(__FUNCTION__, array());
  if (!empty($groups)) {
    return $groups;
  }
  $cache_id = 'blocks';
  $bin = 'vadmin_cache';
  if (!$reset) {
    $cached_data = cache_get($cache_id, $bin);
  }
  if (empty($cached_data->data)) {
    $groups = db_select('vadmin_blocks_group', 'g')
            ->fields('g')
            ->orderBy('g.weight', 'ASC')
            ->execute()->fetchAllAssoc('id');


    foreach ($groups as $group) {
      $gid = $group->id;
      $groups[$gid]->links = vadmin_blocks_get_links($gid);
    }
    cache_set($cache_id, $groups, $bin);
  }
  else {
    $groups = $cached_data->data;
  }
  return $groups;
}

/**
 * Load block by id
 * @param int $id - group id
 * @return object $group or empty array()
 */
function vadmin_blocks_get_group($id) {
  $groups = &drupal_static(__FUNCTION__, array());
  if (empty($groups)) {
    $groups = vadmin_blocks_get_groups();
  }
  return isset($groups[$id]) ? $groups[$id] : array();
}

/**
 * Load function for vadmin_blocks_block -holder
 *
 * @param int $id
 * @return object group
 */
function vadmin_blocks_group_load($id) {
  return vadmin_blocks_get_group($id);
}

/**
 * Load links by bid
 *
 * @param int $bid - id of the link group, if bid is not defined, return all links
 * @return array $link if bid is set, else return all links' array keyed by Bids
 */
function vadmin_blocks_get_links($bid = FALSE) {
  $links = &drupal_static(__FUNCTION__, array());
  if (empty($links)) {
    $db_data = db_select('vadmin_blocks_links', 'l')
            ->fields('l')
            ->execute()->fetchAll();
    $links = array();
    foreach ($db_data as $link) {
      $link->options = unserialize($link->settings);
      unset($link->settings);
      $links[$link->bid][] = $link;
    }
    foreach ($links as &$link) {
      usort($link, '_vadmin_block_sort_links_by_weight');
    }
  }
  if ($bid) {
    return isset($links[$bid]) ? $links[$bid] : array();
  }
  else {
    return $links;
  }
}

/**
 * Load links by link id
 *
 * @param int $bid - id of the link group, if bid is not defined, return all links
 * @return array $link if bid is set, else return the all links' array keyed by Bids
 */
function vadmin_blocks_link_load($lid) {
  $id = (int) $lid;
  if (!$id) {
    return FALSE;
  }
  $db_data = db_select('vadmin_blocks_links', 'l')
          ->condition('l.id', $id)
          ->fields('l')
          ->execute()->fetchAll();
  if ($db_data) {
    $link = array_shift($db_data);
  }
  else {
    return FALSE;
  }
  $link->options = unserialize($link->settings);
  unset($link->settings);
  return $link;
}

/**
 * Alternative link load function (Synonym of vadmin_blocks_link_load)
 * Deprecated in version 1.0
 *
 * @see vadmin_blocks_link_load
 */
function vadmin_blocks_get_link($lid) {
  return vadmin_blocks_link_load($lid);
}

/**
 * Save block group
 * @param object $group containe group data
 *  object methods is
 *     id - update group id, if not set or null new group wil lbe created
 *     name - group name (block subject)
 *     wieght - group weight
 */
function vadmin_blocks_group_save(&$group) {
  if (isset($group->name)) {
    $group->name == trim($group->name);
  }
  if (empty($group->id)) {
    $save = drupal_write_record('vadmin_blocks_group', $group);
  }
  else {
    $save = drupal_write_record('vadmin_blocks_group', $group, 'id');
  }
  $messsage = '';
  if (SAVED_UPDATED == $save) {
    $message = t('Block !name has been updated', array('!name' => $group->subject));
  }
  elseif (SAVED_NEW == $save) {
    $message = t('Block !name has been created', array('!name' => $group->subject));
  }
  cache_clear_all('groups', 'cache_vadmin');
  drupal_set_message($message);
}

/**
 * Delete a block group
 * @param int $gid - id of a group
 * @return bool TRUE if deletion successful or FALSE if an error occured
 */
function vadmin_blocks_block_delete($gid) {
  $deleted_links = db_delete('vadmin_blocks_links')->condition('bid', $gid)->execute();
  $deleted = db_delete('vadmin_blocks_group')->condition('id', $gid)->execute();
  cache_clear_all('groups', 'cache_vadmin');
  return $deleted;
}

/**
 * implements hook_theme()
 */
function vadmin_blocks_theme($existing, $type, $theme, $path) {
  return array(
    'vadmin_blocks_list' => array(
      'render element' => 'form',
      'file' => 'vadmin_blocks.theme.inc'
    ),
    'vadmin_blocks_group' => array(
      'variables' => array(
        'links' => array(),
      ),
      'file' => 'vadmin_blocks.theme.inc',
    )
  );
}

/**
 * Save link in database
 *
 * @param object $link contains all link methods
 *  object methods are
 *     id - updates group id, if not set or null new group will be created
 *     title - link title
 *     href - link href
 *     settings - array of link settings @todo
 *     weight - group weight
 * @param bool $show_message - when true, show message about saved link
 */
function vadmin_blocks_link_save(&$link, $show_message = TRUE) {
  if (isset($link->href) && substr($link->href, 0, 1) === '/') {
    $link->href = substr($link->href, 1);
  }
  if (empty($link->settings)) {
    $link->settings = array();
  }
  if (isset($link->title)) {
    $link->title = trim($link->title);
  }
  if (empty($link->id)) {
    $save = drupal_write_record('vadmin_blocks_links', $link);
  }
  else {
    $save = drupal_write_record('vadmin_blocks_links', $link, 'id');
  }
  if (!isset($link->title)) {
    $link = vadmin_blocks_link_load($link->id);
  }
  cache_clear_all('groups', 'cache_vadmin');
  if ($show_message) {
    $messsage = '';
    if (SAVED_UPDATED == $save) {
      $message = t('Link !name has been updated', array('!name' => $link->title));
    }
    elseif (SAVED_NEW == $save) {
      $message = t('Link !name has been created', array('!name' => $link->title));
    }
    drupal_set_message($message);
  }
}

/**
 * helper function for usort
 * @see usort
 *
 */
function _vadmin_block_sort_links_by_weight($a, $b) {
  if ($a->weight == $b->weight) {
    return 0;
  }
  return ($a->weight > $b->weight) ? 1 : -1;
}

/**
 * Save array of links in db
 *
 * @param array $links - array of objects' links
 */
function vadmin_blocks_links_save($links) {
  foreach ($links as $link_id => $link) {
    if ($link['parent'] > 0) {
      $link_object = new stdClass;
      $link_object->id = $link_id;
      $link_object->bid = $link['parent'];
      $link_object->weight = $link['weight'];
      vadmin_blocks_link_save($link_object, FALSE);
    }
    else {
      $link = vadmin_blocks_get_link($link_id);
      $corrupt_link = print_r($link, TRUE);
      $message = t('Link %link cannot be saved. Parent group is not set. '
          . 'Corrupted link id %link_id', array('%link' => $link->title, '%link_id' => $link_id));
      drupal_set_message($message, 'error');

      watchdog('vadmin', $message, array(), WATCHDOG_WARNING);
    }
  }
}

/**
 * Delete link from db
 *
 * @param int $link_id - link id
 */
function vadmin_blocks_delete_link($link_id) {
  db_delete('vadmin_blocks_links')
      ->condition('id', $link_id)->execute();
}

/**
 * implements hook_block_info()
 */
function vadmin_blocks_block_info() {
  $blocks = array();
  $groups = vadmin_blocks_get_groups();
  foreach ($groups as $group) {
    $blocks['vadmin_blocks_group_' . $group->id] = array(
      'cache' => DRUPAL_CACHE_GLOBAL,
      'info' => $group->subject,
      'weight' => $group->weight
    );
  }
  return $blocks;
}

/**
 * implements hook_block_view()
 */
function vadmin_blocks_block_view($delta = '') {
  $block = array();

  $matches = array();
  if ($gid = _vadmin_blocks_get_byd($delta)) {
    $groups = vadmin_blocks_get_groups();
    if ($group = vadmin_blocks_group_load($gid)) {
      module_load_include('inc', 'vadmin_blocks', 'vadmin_blocks.blocks');
      $view = vadmin_blocks_group_view($group);
      if ($view) {
        $block['content'] = $view;
        $block['subject'] = t($group->subject);
      }
    }
  }
  return $block;
}

/**
 * implements hook_block_info_alter()
 */
function vadmin_blocks_block_info_alter(&$blocks, $theme, $code_blocks) {
  if ($theme == 'voodooedit5' && isset($blocks['vadmin_blocks'])) {
    foreach ($blocks['vadmin_blocks'] as $delta => $block) {
      $blocks['vadmin_blocks'][$delta]['status'] = 1;
      $blocks['vadmin_blocks'][$delta]['region'] = 'sidebar_first';
      if ($gid = _vadmin_blocks_get_byd($delta)) {
        $group = vadmin_blocks_group_load($gid);
        $blocks['vadmin_blocks'][$delta]['weight'] = $group->weight;
      }
    }
  }
}

/**
 * get bid by delta
 */
function _vadmin_blocks_get_byd($delta) {
  if (preg_match("|vadmin_blocks_group_(\d*)|", $delta, $matches)) {
    return $matches[1];
  }
  else {
    return 0;
  }
}

/**
 * Check current user's link path access
 */
function vadmin_blocks_is_access($link) {
  $access = &drupal_static(__FUNCTION__, array());
  if (isset($access[$link->href])) {
    return $access[$link->href];
  }

  $path = drupal_lookup_path($link->href);
  $path = $path ? $path : $link->href;
  $menu_item = menu_get_item($path);
  if (!$menu_item) {
    $access[$link->href] = FALSE;
    return $access[$link->href];
  }
  $access_callback = $menu_item['access_callback'];
  $access[$link->href] = FALSE;
  if ($access_callback !== TRUE) {
    $access_arguments = unserialize($menu_item['access_arguments']);
    if (function_exists($access_callback)) {
      $access[$link->href] = call_user_func_array($access_callback, $access_arguments);
    }else{
      $access[$link->href] = (bool) $access_callback;
    }
  }
  else {
    $access[$link->href] = $access_callback;
  }
  return $access[$link->href];
}
