<?php

function theme_vadmin_blocks_list($variables) {
  $form = $variables['form'];
  $header = array(array('data' => t('Groups'), 'class' => 'groups-col'), t('Weight'), t('Type'), array('data' => t('Actions'), 'class' => 'actions-col'));
  $output = NULL;
  $rows = array();
  $indent = theme('indentation', array('size' => 1));
  foreach (element_children($form['groups']) as $key) {
    $form['groups'][$key]['weight']['#attributes']['class'] = array('item-weight');
    $form['groups'][$key]['current_id']['#attributes']['class'] = array('item-id');
    $form['groups'][$key]['parent']['#attributes']['class'] = array('item-pid');
    $rows[] = array(
      'data' => array(
        drupal_render($form['groups'][$key]['title']),
        drupal_render($form['groups'][$key]['weight']) .
        drupal_render($form['groups'][$key]['current_id']) .
        drupal_render($form['groups'][$key]['parent']),
        t('Group'),
        drupal_render($form['groups'][$key]['actions']),
      ),
      'class' => array('draggable', 'tabledrag-root', 'groups'),
    );
    if (isset($form['links'][$key])) {
      foreach (element_children($form['links'][$key]) as $link_key) {
        $form['links'][$key][$link_key]['weight']['#attributes']['class'] = array('item-weight');
        $form['links'][$key][$link_key]['current_id']['#attributes']['class'] = array('item-id');
        $form['links'][$key][$link_key]['parent']['#attributes']['class'] = array('item-pid');
        $rows[] = array(
          'data' => array(
            $indent . drupal_render($form['links'][$key][$link_key]['title']),
            drupal_render($form['links'][$key][$link_key]['weight']) .
            drupal_render($form['links'][$key][$link_key]['current_id']) .
            drupal_render($form['links'][$key][$link_key]['parent']),
            t('Link'),
            drupal_render($form['links'][$key][$link_key]['actions']),
          ),
          'class' => array('draggable', 'tabledrag-leaf', 'links'),
        );
      }
    }
  }
  $form['new']['weight']['#attributes']['class'] = array('item-weight');

  $rows[] = array(
    'data' => array(
      drupal_render($form['new']['subject']),
      drupal_render($form['new']['weight']),
      t('New group'),
      drupal_render($form['new']['submit']),
    ),
    'class' => array('draggable', 'tabledrag-root'),
  );
  $attributes = array('class' => array('group-tab'), 'id' => 'vadmin-blocks-list');
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => $attributes));
  drupal_add_tabledrag('vadmin-blocks-list', 'match', 'parent', 'item-pid', 'item-pid', 'item-id', FALSE);
  drupal_add_tabledrag('vadmin-blocks-list', 'order', 'sibling', 'item-weight');
  $output .= drupal_render_children($form);
  return $output;
}

function theme_vadmin_blocks_group($variables) {
  $links = $variables['links'];
  $items = array();
  foreach ($links as $link) {
    $items[$link->id] = l($link->title, $link->href, $link->options);
  }
  $attributes = array(
    'class' => array(
      'vadmin_blocks-block-link',
    ),
  );
  $html = theme('item_list', array('items' => $items, 'type' => 'ul', 'attributes' => $attributes));
  return $html;
}
