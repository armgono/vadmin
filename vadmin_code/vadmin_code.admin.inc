<?php

function vadmin_code_settings_form($form, $form_state) {
  $form['vadmin_code_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Code to header'),
    '#default_value' => vadmin_get_data('vadmin_code_header', ''),
    '#description' => t('Create code block and put it in page <head> tag (before scripts)')
  );
  $form['vadmin_code_top'] = array(
    '#type' => 'textarea',
    '#title' => t('Code after body'),
    '#default_value' => vadmin_get_data('vadmin_code_top', ''),
    '#description' => t('Create code block and append it to page  <body> tag ($page_top region)')
  );

  $form['vadmin_code_bottom'] = array(
    '#type' => 'textarea',
    '#title' => t('Code before /body'),
    '#default_value' => vadmin_get_data('vadmin_code_bottom', ''),
    '#description' => t('Create code block and append it to body tag. ($page_bottom region)')
  );
  $form['vadmin_code_var'] = array(
    '#type' => 'textarea',
    '#title' => t('Code to variable. Variable name is $vcode'),
    '#default_value' => vadmin_get_data('vadmin_code_var', ''),
    '#description' => t('Create code block and save it in variable $vcode. This variable is accessible in html.tpl.php and page.tpl.php files as &lt;?php print $vcode; ?&gt')
  );
  $themes_list = system_rebuild_theme_data();
  $options = array();
  foreach(array_filter($themes_list, '_vadmin_code_theme_filter') as $theme){
    $options[$theme->name] = $theme->info['name'];
  }
  $form['vadmin_code_themes'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#title' => t('Select themes, where you want show code above'),
    '#default_value' => vadmin_get_data('vadmin_code_themes',array())
  );
  $form['#submit'][] = 'vadmin_code_submitter';
  return vadmin_settings_form($form);
}
/**
 * array_filter callback function, for enabled filtering themes
 *
 * @param type $theme
 */
function _vadmin_code_theme_filter($theme){
  if($theme->status){
    return TRUE;
  }else{
    return FALSE;
  }
}

/**
 * custom submit_function
 */

function vadmin_code_submitter(&$form, &$form_state){
  variable_set('vadmin_code_themes', $form_state['values']['vadmin_code_themes']);
}