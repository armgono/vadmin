<?php

/**
 * Text field view widget
 *
 * @param object $data - object of query if $type = 'query' or object of row items if $type = 'view'
 * @param string $type - type of calling this function. Variable contains params
 *                     - query - for altering field query
 *                     - view - for generating view data
 * @param mixed $params - advanced params for query altering or view
 * @return mixed $data query object if $type = 'query' or html data if $type = 'view'
 */
function vadmin_content_field_view_text($data, $type = 'view', $params = NULL) {
  switch ($type) {
    case 'query':
      $field_name = $params['field_name'];
      $data->leftJoin('field_data_' . $field_name, $field_name, $field_name . '.entity_id = n.nid');
      $data->addField($field_name, $field_name . '_value', $field_name);
      return $data;
      break;
    case 'view':
      $text = strip_tags($data->{$params});
      return truncate_utf8(filter_xss_admin($text), 150, FALSE, TRUE);
      break;
  }
}

/**
 * Image field view widget
 *
 * @see vadmin_content_field_view_text()
 */
function vadmin_content_field_view_image($data, $type = 'view', $params = NULL) {
  switch ($type) {
    case 'query':
      $field_name = $params['field_name'];
      $data->leftJoin('field_data_' . $field_name, $field_name, $field_name . '.entity_id = n.nid AND ' . $field_name . '.delta = 0');
      $data->addField($field_name, $field_name . '_fid', $field_name);
      return $data;
      break;
    case 'view':
      $fid = (int) $data->{$params};
      if ($fid) {
        $image_file = file_load($fid);
        $style = array(
          'style_name' => 'vadmin_content_preview',
          'path' => $image_file->uri
        );
        return theme('image_style', $style);
      }
      else {
        return t('Not set');
      }
      break;
  }
}

/**
 * Custom fields' view widget
 *
 * @see vadmin_content_field_view_text()
 */
function vadmin_content_field_view_field($data, $type = 'view', $params = NULL) {
  switch ($type) {
    case 'query':
      return $data;
      break;
    case 'view':
      $terms = field_view_field('node', $data->node, $params, array('label' => 'hidden'));
      return render($terms);
      break;
  }
}
