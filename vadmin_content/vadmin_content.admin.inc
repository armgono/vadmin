<?php

/**
 * Content types' select form.
 * Selected content types will be shown in the left blocks
 */
function vadmin_content_form($form, $form_state) {
  $form = array();
  $node_types = node_type_get_types();
  $options = array();
  foreach ($node_types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select types which are accessible on the left menu'),
    '#options' => $options,
    '#default_value' => vadmin_get_data('content_types', array())
  );
  return vadmin_settings_form($form);
}

/**
 * Content types' fields select form
 */
function vadmin_content_fields_form($form, $form_state) {
  $form = array();
  $content_types = array_filter(vadmin_get_data('content_types'));
  if (!$content_types) {
    return array();
  }
  $node_types = node_type_get_types();
  $form['intro'] = array(
    '#type' => 'markup',
    '#weight' => -50,
    '#markup' => '<h3>' . t('Select fields, which will be displayed on content page') . '</h3>'
  );
  $defaults = vadmin_get_data('content_fields', array());
  foreach ($content_types as $type) {
    $weight = isset($defaults[$type]['_weight']) ? $defaults[$type]['_weight'] : 0;
    $node_instatnces = field_read_instances(array('bundle' => $type));
    $form[$type] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#title' => $node_types[$type]->name,
      '#weight' => $weight
    );
    foreach ($node_instatnces as $field) {
      $field_name = $field['field_name'];
      $field_info = field_info_field($field_name);
      $form[$type]['_custom_link'] = array(
        '#type' => 'textfield',
        '#title' => t('Custom link'),
        '#default_value' => isset($defaults[$type]['#custom_link']) ? $defaults[$type]['#custom_link'] : '',
        '#description' => t('Link to custom page for shoing this content type')
      );
      $form[$type]['_weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#delta' => 25,
        '#default_value' => $weight,
      );
      $form[$type][$field_name] = array(
        '#title' => $field['label'],
        '#type' => 'checkbox',
        '#default_value' => isset($defaults[$type][$field_name]) ? $defaults[$type][$field_name] : 0
      );
    }
  }
  $form['form-actions'] = array('#type' => 'actions');
  $form['form-actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

function vadmin_content_fields_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = array_filter(array_map('array_filter', $form_state['values']));
  vadmin_set_data('content_fields', $values);
  drupal_set_message(t('FIelds\' settings has been saved.'));
}
