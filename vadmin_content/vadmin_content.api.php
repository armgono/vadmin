<?php

/**
 * @file
 * Hooks provided by the Vadmin content module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter content page view vidgets
 *
 * @param $allowed_types
 * an associative array of field_type widget settings contain keys
 *  - title: Field title,
 *  - callback: Callback_function_name
 *  - file: file name, without extension, default is 'plugins/widget',
 *  - file type: file extension, default is 'inc'
 *  - file path: includes file location, default is vadmin_blocks path
 */
function hook_vadmin_content_allowed_types_alter($allowed_types) {
  $allowed_types['my_widget_type'] = array(
    'title' => 'My title',
    'callback' => 'my_widget_callback',
    'file' => 'myfileName',
    'file type' => 'inc',
    'file path' => drupal_get_path('module', 'mymodule')
  );
}

/**
 * @} End of "addtogroup hooks".
 */
