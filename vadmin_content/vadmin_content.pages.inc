<?php

/**
 * Content view page generation function
 */
function vadmin_content_main_page($fields) {
  $filters = array(); // Filters block before content
  $url_query = drupal_get_query_parameters(); //Save current page url parameters
  $current_path = request_path();
  $header = array();
  $header[] = array('data' => t('On site'));
  $header[] = array('data' => $fields['#title_label'], 'field' => 'n.title');

  // Add i18n support
  $multilangual = FALSE;
  if (module_exists('i18n')) {
    $header[] = array('data' => t('Language'), 'field' => 'n.language');
    $languages = language_list();
    $multilangual = TRUE;
    $filters['lang']['#weight'] = 10;
    $filters['lang']['#prefix'] = '<p class="language-select">';
    $filters['lang']['#suffix'] = '</p>';
    foreach ($languages as $lang => $language) {
      $lang_query = array('lang' => $lang) + $url_query;
      $query_string = drupal_http_build_query($lang_query);
      $is_current_lang = (isset($url_query['lang']) && $url_query['lang'] == $lang);
      $href = $current_path . '?' . $query_string;
      $attributes = $is_current_lang ? array('class' => array('active')) : array();
      $filters['lang'][$lang] = array(
        '#markup' => '<a href="/' . $href . '"' . drupal_attributes($attributes) . '>' . $language->native . '</a>',
        '#suffix' => ' | '
      );
    }
    $attributes = empty($url_query['lang']) ? array('class' => array('active')) : array();

    $filters['lang']['all'] = array(
      '#markup' => '<a href="/' . $current_path . '"' . drupal_attributes($attributes) . '>' . t('All') . '</a>'
    );
  }

  $header[] = array('data' => t('Created'), 'field' => 'n.created', 'sort' => 'desc');
  $limit = vadmin_get_data('vadmin_content_limit', 50);
  if (isset($_GET['per-page'])) {
    if ($custom_limit = (int) $_GET['per-page']) {
      $limit = $custom_limit;
    }
  }
  $query = db_select('node', 'n')
      ->condition('n.type', $fields['#type'])
      ->extend('PagerDefault')
      ->limit($limit)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->fields('n', array('nid', 'title', 'created', 'status'));
  if (isset($url_query['lang'])) {
    $query->condition('n.language', $url_query['lang']);
  }
  $field_names = element_children($fields);
  //Extend query for new fields
  $allowed_types = vadmin_content_extend_fields($query, $header, $fields, $field_names);

  $header[] = array('data' => t('Actions'));

  $result = $query->execute();
  $rows = array();
  $status_opts = array();
  $status_opts['html'] = TRUE;
  $status_opts['attributes']['class'] = array('status-change', 'use-ajax');
  $title_opts['attributes']['class'][] = 'node-title';

  $actions_query = $url_query;
  $actions_query['destination'] = request_path();
  foreach ($result as $data) {
    $nid = $data->nid;
    $data->node = node_load($nid);
    if ($data->status) {
      $status_class = 'publish';
      $status_opts['attributes']['title'] = t('Unpublish');
    }
    else {
      $status_class = 'unpublish';
      $status_opts['attributes']['title'] = t('Publish');
    }

    $status_attributes = array(
      'id' => 'node-status-' . $data->nid,
      'class' => array($status_class, 'icon')
    );
    $status_link = 'ajax/vadmin/cahnge-status/' . $nid . '/nojs';
    $status_title = '<span' . drupal_attributes($status_attributes) . '></span>';
    $status = l($status_title, $status_link, $status_opts);
    if ($multilangual) {
      if ($data->node->language !== 'und') {
        $lang_suffix = $languages[$data->node->language]->native;
      }
      else {
        $lang_suffix = t('Neutral');
      }
      $rows[$data->nid] = array(
        'data' => array(
          $status,
          l($data->title, 'node/' . $nid, $title_opts),
          $lang_suffix,
          format_date($data->created, 'custom', 'd.m.Y - h.i'),
        ),
        'class' => array($status_class),
        'id' => 'row-' . $nid
      );
    }
    else {
      $rows[$data->nid] = array(
        'data' => array(
          $status,
          l($data->title, 'node/' . $nid, $title_opts),
          format_date($data->created, 'custom', 'd.m.Y - h.i'),
        ),
        'class' => array($status_class),
        'id' => 'row-' . $nid
      );
    }
    //Fields
    foreach ($field_names as $field_name) {
      $field = $fields[$field_name];
      if (empty($allowed_types[$field['type']])) {
        $allowed_type = $allowed_types['custom'];
      }
      else {
        $allowed_type = $allowed_types[$field['type']];
      }
      $callback = $allowed_type['callback'];
      if (function_exists($callback)) {
        $rows[$nid]['data'][] = call_user_func_array($callback, array($data, 'view', $field_name));
      }
    }
    //--End fields
    //Add actions
    $actions = array();
    $actions['edit'] = l(t('Edit'), 'node/' . $nid . '/edit', array('query' => $actions_query));
    $actions['delete'] = l(t('Delete'), 'node/' . $nid . '/delete', array('query' => $actions_query));
    $rows[$nid]['data'][] = implode(' | ', $actions);
  }
  $filters['add_link'] = array(
    '#weight' => 0,
    '#markup' => l(t('Add !type', array('!type' => $fields['#name'])), 'node/add/' . str_replace('_', '-', $fields['#type']), array('query' => $actions_query)),
    '#prefix' => '<p class="add-link">',
    '#suffix' => '</p>'
  );
  $table = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#sticky' => TRUE,
    '#empty' => t('Content does not exist'),
    '#attached' => array(
      'library' => array(
        array('system', 'drupal.ajax')
      )
    ),
    '#attributes' => array(
      'class' => array(
        'vadmin-content-table'
      )
    )
  );
  $pager = array(
    '#theme' => 'pager',
    '#tags' => array()
  );
  $search_form = drupal_get_form('vadmin_content_search_filter', $fields['#type']);
  return array($filters, $search_form, $table, $pager);
}

/**
 * change node_status
 */
function vadmin_content_ajax_change_status($node, $mode) {
  $old_status = $node->status;
  $node->status = $node->status ? 0 : 1;
  $new_status = $node->status;
  node_save($node);
  if ($mode == 'nojs') {
    if (isset($_GET['destination'])) {
      drupal_goto($_GET['destination']);
    }
    else {
      drupal_goto('admin/content/vadmin/' . $node->type);
    }
  }
  $classes = array(
    'unpublish',
    'publish'
  );
  $commands = array();
  $commands[] = ajax_command_invoke('#node-status-' . $node->nid, 'addClass', array($classes[$new_status]));
  $commands[] = ajax_command_invoke('#node-status-' . $node->nid, 'removeClass', array($classes[$old_status]));

  if ($new_status) {
    $commands[] = ajax_command_invoke('#row-' . $node->nid . ' .status-change', 'attr', array('title', t('Unpublish')));
    $commands[] = ajax_command_invoke('#row-' . $node->nid, 'removeClass', array('unpublish'));
  }
  else {
    $commands[] = ajax_command_invoke('#row-' . $node->nid . ' .status-change', 'attr', array('title', t('Publish')));
    $commands[] = ajax_command_invoke('#row-' . $node->nid, 'addClass', array('unpublish'));
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}
