<?php

function vadmin_defaults_form($form, $form_state) {
  $form = array();
  $form['info'] = array(
    '#type' => 'markup',
    '#markup' => t('Set default setting for site, use this if you do not configure system modules')
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set defaults')
  );
  $roles = variable_get('imce_roles_profiles', array());

  return $form;
}

function vadmin_defaults_form_submit(&$form, &$form_state) {
  $vars = vadmin_defaults_get_defautls();
  $break = "\n";
  foreach ($vars as $module_name => $module) {
    $out[$module_name] = array(
      '#prefix' => '- ' . t('Set values %title', array('%title' => $module['title'])) . $break,
      '#suffix' => $break
    );
    foreach ($module['set'] as $varName => $varData) {
      variable_set($varName, $varData['#value']);
      $out[$module_name]['message'][]['#markup'] = '-- ' . $varData['#message'] . $break;
    }
  }
  drupal_set_message(nl2br(render($out)));
}
