<?php

/**
 * @file vadmin_defaults.api.php
 *
 * hooks and alters examples
 */

/**
 * You can set defaults' settings for any modules, and use it, when submitted to vadmin_defaults' setting
 */
function hook_vadmin_defaults_alter(&$vars) {
  $vars['myModule'] = array(
    'title' => t('Set custom module default value'),
    'set' => array(
      'variable_key' => array(
        '#value' => 'Custom value',
        '#message' => t('variable_key value now is Custom value')
      )
    )
  );
}
