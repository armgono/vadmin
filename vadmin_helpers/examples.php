<?php

/**
 * @file Exaples for using VoodooHelper API
 */

/**
 * Adding Youtube video to node fields and replace screenshot
 * 
 * Show video for node_type to any node_view call (ful lpage, teaser and more)
 * implements hook_node_view_alter()
 */
function MYMODULE_node_view_alter(&$build) {
  if ($build['#entity_type'] == 'node' && $build['#bundle'] = 'page' && !empty($build['field_video'])) {
    $wrapper = entity_metadata_wrapper('node', $build['#node']);
    $youtube = new voodooApiYoutube($wrapper->field_video->value());
    $build['field_video'][0]['#markup'] = $youtube->getIframe();
  }
}

/**
 * Altering node add form
 * Add advanced submit function to our node edit form
 * You can alter any node form, where exists youtube video link field 
 * and screenshot
 */
function MYMODULE_form_page_node_form_alter(&$form, &$form_state) {
  $form['#submit'][] = 'MYMODULE_set_screenshot';
}

/**
 * Generate screensfhot and append it to image field
 */
function MYMODULE_set_screenshot(&$form, &$form_state) {
  $values = $form_state['values'];
  if ($values['field_video']['und'][0]['value'] && !$values['field_photo']['und'][0]['fid']) {
    $youtube = new voodooApiYoutube($values['field_video']['und'][0]['value']);
    $image = $youtube->getScreenshot();
    $form_state['values']['field_photo']['und'][0] = (array) $image;
  }
}
