<?php

class voodooApi {

  /**
   * Возвращает скриншот видео из vimeo
   * @param string $id ID видео из сервиса Vimeo
   * @param string $type - тип скриншота, может принимать следующие значения 'small', 'medium', 'large'
   *                     - по умолчанию 'medium'
   * @return boolean
   */
  public static function get_vimeo_screenshot($id, $type = 'medium') {
    if (!is_numeric($id))
      return FALSE;
    $type = 'thumbnail_' . $type;
    $info = &drupal_static(__FUNCTION__, array());
    if (!isset($info[$id])) {

      $page = "http://vimeo.com/api/v2/video/{$id}.php";

      $file = @file_get_contents($page);
      if (!$file) {
        $message = t('Video is not available on vimeo.com');
        drupal_set_message($message, 'warning');
        watchdog('voodooAPI', $message, array(), WATCHDOG_WARNING);
        return FALSE;
      }
      $tmp = unserialize($file);
      if (!empty($tmp) && is_array($tmp)) {
        $info[$id] = array_shift($tmp);
      }
    }
    if (isset($info[$id][$type])) {
      return $info[$id][$type];
    }
    else
      return FALSE;
  }

  /**
   * Проверяет соответствие текущего пути с указаным паттерном
   *
   * @param mixed $path_patterns - строка разделенная \n или массив путей для проверки
   * @return bool TRUE если текущий путь соответствует шаблону
   */
  public static function check_path($path_patterns) {
    if (is_array($path_patterns)) {
      $pages = drupal_strtolower(implode("\n", $path_patterns));
    }
    else {
      $pages = drupal_strtolower($path_patterns);
    }
    $current_path = drupal_strtolower(drupal_get_path_alias($_GET ['q']));
    $page_match = drupal_match_path($current_path, $pages);
    if ($current_path != $_GET ['q']) {
      $page_match = $page_match || drupal_match_path($_GET ['q'], $pages);
    }
    return $page_match;
  }

}

class voodooApiBlocks {

  public $blocks;
  private $defaults = array(
    'info' => array(
      'cache' => DRUPAL_CACHE_GLOBAL
    ),
    'view' => array(
      'subject' => NULL,
      'callback' => NULL,
      'content' => array(),
      'callback arguments' => array(),
      'title calback' => NULL,
      'called' => FALSE,
    )
  );

  public function __construct() {

  }

  /**
   * обработчики для hook_block_info()
   */
  public function create($delta, $title, $block = array()) {
    $this->blocks['info'][$delta] = $block + $this->defaults['info'];
    $this->blocks['info'][$delta]['info'] = t($title);
  }

  /**
   * обработчики для hook_block_view()
   */
  public function set_view($delta, $content, $block = array()) {
    $this->blocks['view'] = &drupal_static(__FUNCTION__, array());
    if (empty($this->blocks['view'][$delta])) {
      $this->blocks['view'][$delta] = $block + $this->defaults['view'];
      if (is_string($content) && function_exists($content)) {
        $this->blocks['view'][$delta]['callback'] = $content;
      }
      else {
        $this->blocks['view'][$delta]['content'] = $content;
      }
      if ($this->blocks['view'][$delta]['callback']) {
        $this->blocks['view'][$delta]['content'] = call_user_func_array($this->blocks['view'][$delta]['callback'], $this->blocks['view'][$delta]['callback arguments']);
      }
    }
  }

  public function get($type = 'info', $delta = '') {
    switch ($type) {
      case 'info':
        return isset($this->blocks['info']) ? $this->blocks['info'] : array();
        break;
      case 'view':
        $callback = $this->blocks['view'][$delta]['callback'];
        $called = $this->blocks['view'][$delta]['called'];
        if ($called && function_exists($callback) && empty($this->blocks['view'][$delta]['content'])) {
          $arguments = $this->blocks['view'][$delta]['callback arguments'];
          $this->blocks['view'][$delta]['content'] = call_user_func_array($callback, $arguments);
          $this->$this->blocks['view'][$delta]['called'] = TRUE;
        }
        return array(
          'content' => $this->blocks['view'][$delta]['content'],
          'subject' => $this->blocks['view'][$delta]['subject']
        );
        break;
    }
    return array();
  }

}
