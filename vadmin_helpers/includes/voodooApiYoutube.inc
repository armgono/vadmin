<?php

/**
 * @file содержит класс для работы с youtube.
 */
class voodooApiYoutube {

  private $id;
  private $url;
  private $screenshotUrl;
  public $screenshot;
  public $iframe;

  public function __construct($url = '') {
    if ($url) {
      $this->setUrl($url);
    }
  }

  public function setUrl($url) {
    $this->url = $url;
    $this->generateId();
  }

  public function getScreenshot($path = 'public://') {
    $this->getScreenshotUrl();
    $screenshot = system_retrieve_file($this->screenshotUrl, $path, TRUE, FILE_EXISTS_REPLACE);
    $this->screenshot = file_move($screenshot, $path . '/' . $this->id . '.jpg');
    return $this->screenshot;
  }

  public function getIframe($width = 600, $height = 400) {
    if (empty($this->iframe)) {
      $this->iframe = '<iframe width="' . $width . '" height="' . $height . '" src="https://www.youtube.com/embed/' . $this->id . '" frameborder="0" allowfullscreen></iframe>';
    }
    return $this->iframe;
  }

  private function generateId() {
    $url_parts = parse_url($this->url);
    $host = str_replace('www.', '', $url_parts['host']);
    if ($host == 'youtube.com') {
      $query_array = drupal_get_query_array($url_parts['query']);
      $this->id = $query_array['v'];
    }
    elseif ($host == 'youtu.be') {
      $path = drupal_substr($url_parts['path'], 1);
      if (!empty($path)) {
        $this->id = $path;
      }
    }

    if (empty($this->id)) {
      watchdog('VH', 'Url is not correct youtube video url');
    }
  }

  private function getScreenshotUrl() {
    $this->screenshotUrl = 'http://img.youtube.com/vi/' . $this->id . '/0.jpg';
  }

  public function getUrl() {
    return 'https://www.youtube.com/embed/' . $this->id;
  }

}
