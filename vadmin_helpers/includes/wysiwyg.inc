<?php

function _vadmin_helper_get_wysiwyg_defaults($key = 'buttons') {
  $data['buttons'] = 'a:3:{s:7:"default";a:22:{s:4:"Bold";i:1;s:6:"Italic";i:1;s:9:"Underline";i:1;s:6:"Strike";i:1;s:11:"JustifyLeft";i:1;s:13:"JustifyCenter";i:1;s:12:"JustifyRight";i:1;s:12:"JustifyBlock";i:1;s:12:"BulletedList";i:1;s:12:"NumberedList";i:1;s:4:"Link";i:1;s:6:"Unlink";i:1;s:5:"Image";i:1;s:9:"PasteText";i:1;s:13:"PasteFromWord";i:1;s:12:"RemoveFormat";i:1;s:6:"Format";i:1;s:6:"Styles";i:1;s:5:"Table";i:1;s:5:"Flash";i:1;s:9:"CreateDiv";i:1;s:6:"Iframe";i:1;}s:7:"youtube";a:1:{s:7:"Youtube";i:1;}s:4:"imce";a:1:{s:4:"imce";i:1;}}';

  return isset($data[$key]) ? $data[$key] : '';
}

