(function ($) {
  Drupal.behaviors.vadminHelpersDTP = {
    attach: function (context, settigns) {
      $.datetimepicker.setLocale('ru');
      $('#dtp input').datetimepicker({
        format: 'Y-m-d H:i',
        mask:true
      });
    }
  };
})(jQuery);