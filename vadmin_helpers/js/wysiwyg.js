/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
  Drupal.behaviors.vadminHelpersWysiwyg = {
    attach: function (context, settings) {

      if (settings.vh_buttons === undefined) {
        $('#vh-defautls').remove();
        return false;
      }
      var buttons = settings.vh_buttons;
      $('#vh-defautls').on('click', 'a', function (e) {
        e.preventDefault();
        for (var group  in buttons) {
          if (buttons.hasOwnProperty(group) && (typeof buttons == 'object')) {
            for (var key in buttons[group]) {
              if (buttons[group].hasOwnProperty(key)) {
                var name = 'buttons[' + group + '][' + key + ']';

                $('input[name="' + name + '"]').attr('checked', 'checked').val(1);
              }
            }
          }
        }
      });
    }
  };
})(jQuery);