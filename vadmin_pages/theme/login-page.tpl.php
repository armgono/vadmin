<div class="page">
  <div id="cont">

    <div id="login">
      <div id="branding">
        <span class="cms"></span>
        <span class="drupal"></span>
      </div>
      <h1><?php print $title; ?></h1>
      <?php print render($page['content']); ?>
      <?php print $messages; ?>
    </div>
  </div>
</div>