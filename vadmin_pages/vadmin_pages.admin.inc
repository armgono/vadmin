<?php

/**
 * vadmin pages settings
 * Features:
 * Enable/disable user login page
 */
function vadmin_pages_settings_form($form, $form_state) {
  $form = array();
  $form['info'] = array(
    '#tree' => FALSE,
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('About site'),
    '#collapsed' => TRUE
  );
  $form['info']['company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company name'),
    '#default_value' => vadmin_get_data('company_name', variable_get('site_name', '')),
    '#description' => t('Set company name for support mails. ex: LTD MyCompany')
  );
  $form['info']['copyright'] = array(
    '#type' => 'textfield',
    '#title' => t('Copyright'),
    '#default_value' => vadmin_get_data('copyright', ''),
    '#description' => t('Set © copyright for company, you can use !year placeholder for current year. This data available in template files as var $copyright.')
  );
  $markdown_link = l('markdown', 'http://daringfireball.net/projects/markdown/', array(
    'external' => TRUE,
    'attributes' => array(
      'target' => '_balnk'
    )
      )
  );

  $form['instructions'] = array(
    '#type' => 'managed_file',
    '#title' => t('Instructions'),
    '#desription' => t('Upload instructions file, for administrators'),
    '#upload_location' => 'public://vadmin/',
    '#default_value' => vadmin_get_data('instructions', ''),
  );
  $defaults_link = l(t('Load default'), request_path(), array('query' => array('default' => 1)));
  $default_text = t('You can load !default instrunctions. But this overrides current data', array('!default' => $defaults_link));
  if (isset($_GET['default']) && ($_GET['default'] == 1)) {
    vadmin_set_data('vadmin_pages_main', vadmin_pages_load_default_instructions());
    drupal_goto(request_path());
  }
  $form['vadmin_pages_main'] = array(
    '#title' => t('Main page instructions'),
    '#description' => t('User the !markdown syntax.', array('!markdown' => $markdown_link)) . '<br>' . $default_text,
    '#type' => 'textarea',
    '#default_value' => vadmin_get_data('vadmin_pages_main', ''),
  );

  $form['settings'] = array(
    '#tree' => FALSE,
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Settings'),
    '#collapsed' => TRUE
  );
  $form['settings']['vadmin_pages_user'] = array(
    '#title' => t('Enable /user login page'),
    '#default_value' => vadmin_get_data('vadmin_pages_user', 0),
    '#type' => 'checkbox',
  );
  $form['#submit'][] = 'vadmin_pages_settings_form_submit';
  return vadmin_settings_form($form);
}

function vadmin_pages_admin_page() {
  $raw_content = vadmin_get_data('vadmin_pages_main', '');
  $out = array();

  $out['content'] = array(
    '#theme' => 'vadmin_pages_markdown',
    '#content' => $raw_content,
  );
  return $out;
}

function vadmin_pages_settings_form_submit(&$form, &$form_state) {
  $file = file_load($form_state['values']['instructions']);
  if (isset($form_state['values']['copyright'])) {
    variable_set('vadmin_copyright', $form_state['values']['copyright']);
  }
  if ($file) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'vadmin_pages', 'instruction', 1);
  }
}
