<?php
function vadmin_pages_login_page(){
  if(user_is_logged_in()){
    vadmin_pages_redirect_directly();
    drupal_exit();
  }
  $login_block = drupal_get_form('user_login');
  return $login_block;
}