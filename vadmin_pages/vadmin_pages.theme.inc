<?php

function theme_vadmin_pages_markdown($variables) {
  $instuction = vadmin_get_data('instructions', 0);
  $replacement = array();

  if (($file = file_load($instuction)) && user_access('vadmin pages access instructions')) {
    $link = file_create_url($file->uri);
    $replacement['!instruction'] = l(t('with instruction'), $link);
  }
  else {
    $replacement['!instruction'] = t('with instruction');
  }
  $replacement['!support'] = l(t('to support'), 'http://support.voodoo.ru');
  global $base_url;
  $url = str_replace(array('http://www.', 'http://'), 'www.', $base_url);
  $company_name = vadmin_get_data('company_name', variable_get('site_name', ''));
  $mail_subject = $company_name . '. ' . t('Site') . ' ' . $url;
  $replacement['!mail'] = '<a href="mailto:support@voodoo.ru; info@voodoo.ru?subject='
      . $mail_subject . '">' . t('message') . '</a>';
  $out = '';

  $text = 'Hi !username. You are in the site administrative control panel. '
      . 'Before start working read the !instruction. '
      . 'If after reading the instructions you still have questions,  pleaset '
      . 'contact !support or write !mail to us.';
  global $user;
  $account = user_load($user->uid);
  //Realname integration
  if (isset($account->realname)) {
    $replacement['!username'] = $account->realname;
  }
  else {
    $replacement['!username'] = $account->name;
  }
  $out .= '<p>' . t($text, $replacement) . '</p>';
  $out .='<h2>' . t('Instructions') . '</h2>';
  module_load_include('inc', 'vadmin_pages', 'includes/markdown');
  $out .= Markdown($variables['content']);
  return $out;
}
