<?php

function vadmin_webform_settings_form() {
  $form = array();
  $types = node_type_get_names();
  $selected_types = vadmin_get_data('vadmin_webform_types', array());
  $form['vadmin_webform_types'] = array(
    '#title' => t('Select webform node types'),
    '#options' => $types,
    '#type' => 'checkboxes',
    '#default_value' => $selected_types
  );
  $form['array_filter'] = array(
    '#type' => 'value',
    '#value' => TRUE
  );
  return vadmin_settings_form($form);
}

function vadmin_webform_fields_form() {
  $form = array();

  $defaults = vadmin_get_data('vadmin_webform_fiels', array());
  $selected_types = vadmin_get_data('vadmin_webform_types', '');

  $webforms = array();
  foreach ($selected_types as $type) {
    $nodes = node_load_multiple(array(), array('type' => $type));
    foreach ($nodes as $node) {
      $webforms[$node->language][$node->nid] = $node;
    }
  }
  $languages = language_list();

  foreach ($webforms as $lang => $webform) {
    $form[$lang] = array(
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => isset($languages[$lang]) ? $languages[$lang]->native : t('Language neutral')
    );
    foreach ($webform as $nid => $item) {
      $options = array();
      foreach ($item->webform['components'] as $component) {
        $options[$component['cid']] = $component['name'];
      }
      $form[$lang][$nid] = array(
        '#type' => 'checkboxes',
        '#title' => $item->title,
        '#options' => $options,
        '#default_value' => isset($defaults[$nid]) ? $defaults[$nid] : array()
      );
    }
  }
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

function vadmin_webform_fields_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = array_filter(array_map('array_filter', $form_state['values']));
  vadmin_set_data('vadmin_webform_fiels', $values);
  drupal_set_message(t('All data has saved'));
}
