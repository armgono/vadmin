<?php

function vadmin_webform_results($node) {
  $defaults = vadmin_get_data('vadmin_webform_fiels', array());
  if (empty($defaults[$node->nid])) {
    drupal_not_found();
  }
  $cids = $defaults[$node->nid];

  module_load_include('inc', 'webform', 'includes/webform.submissions');
  $submittions = webform_get_submissions(array('nid' => $node->nid));
  $header = array();
  $header[] = t('#');
  foreach ($cids as $cid) {
    if (isset($node->webform['components'][$cid])) {
      $header[] = $node->webform['components'][$cid]['name'];
    }
  }
  $header[] = t('Submitted');
  $header[] = t('Actions');
  $i = count($submittions);
  $rows = array();
  foreach (array_reverse($submittions) as $submittion) {
    $num = $submittion->sid;
    $rows[$num] = array();
    $rows[$num][] = format_string('!num/!gnum', array('!num' => $i--, '!gnum' => $num));
    foreach ($cids as $cid) {
      if (isset($submittion->data[$cid])) {
        $rows[$num][] = implode("<br>", $submittion->data[$cid]);
      }else{
        $rows[$num][] = t('Not set');
      }
    }
    $rows[$num][] = format_date($submittion->submitted, 'custom', 'd.m.Y h:i');
    $rows[$num][] = l(t('read more'), 'node/' . $node->nid . '/submission/' . $num);
  }

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows
  );
}

function vadmin_webform_results_title($node) {
  $title = format_string('Webform !form results', array('!form' => $node->title));
  return $title;
}
